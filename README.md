# README #

This is a SDL Dynamic Camera Sample to keep camera focused on multiple players
It includes a basic GameObject class and creates 4 players during init

### What is this repository for? ###

* Simple SDL Dynamic Player(s) follow camera with zoom scaling
* Zooming should be done with an easing function the current scale is there as a placeholder
* SDL TTF is just used for visual debug

### How do I get set up? ###

* Clone repository
* Download Development Libraries for  Windows 32-bit OR 64-bit
* Set Environment Variables using this guide https://support.microsoft.com/en-us/kb/310519
* Download **SDL** from https://www.libsdl.org/download-2.0.php
	* Set environment variable for SDL
	* Alternatively `SET SDL_SDK="C:\####\SDL_###"` directory location
* Download **SDL Image** from https://www.libsdl.org/projects/SDL_image/	
	* Set environment variable for SDL Image
	* Alternatively `SET SDL_IMAGE_SDK="C:\Users\#####\####\SDL_Image_###"` directory location
* Download **cute_c2** from https://github.com/RandyGaul/cute_headers
	* Set environment variable for CUTE
	* Alternatively `SET CUTE_SDK="C:\####\CUTE_###"` directory location
* To check environment variable is set correctly open a command prompt and type `echo %GLEW_SDK%` the path to glew sdk should be show.
* Select a project default target `x86` when running executable
* If the project builds but does not `xcopy` the required dll's try moving your project to a directory you have full access to, see http://tinyurl.com/SFMLStarter for a guide on post build events.
* Alternatively set the Environment Variable in Configuration Properties | Debugging | Environment to `PATH=%PATH%;%SFML_SDK%\bin;%GLEW_SDK%\bin\Release\Win32` this will ensure DLL's are discoverable when running in debug mode with copying the DLL's to the executable directory

### Cloning Repository ###
* Run GitBash and type the Follow commands into GitBash

* Check Present Working Directory `pwd`

* Change to the C drive or other drive `cd c:`

* Make a projects Directory `mkdir projects`

* Change to the projects directory by `cd projects`

* Clone the project `git clone https://MuddyGames@bitbucket.org/MuddyGames/sdl_dynamic_camera.git`

* Change to the project directory `cd projects sdl_dynamic_camera.git`

* List files that were downloaded `ls`

### Who do I talk to? ###

* philip.bourke@itcarlow.ie