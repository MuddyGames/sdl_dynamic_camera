#include <Debug.h>

#define CUTE_C2_IMPLEMENTATION
#include <cute_c2.h>

#include <SDL.h>
#undef main

#include <time.h>
#include <iostream>
#include <string>

#include <algorithm>

using namespace std;

#define SCREEN_WIDTH		800
#define SCREEN_HEIGHT		600
#define LEVEL_WIDTH			1600
#define LEVEL_HEIGHT		1200

#define CHARACTER_WIDTH		16
#define CHARACTER_HEIGHT	16

#define MAX					4

const int PLAYERS = 4; // Min 2
const int COLLIDERS = 50;  // Min 2

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;
SDL_Rect* focus = NULL;
SDL_Rect* offset = NULL;

float scale = 1.0f;

struct Timer
{
	Uint64 previous_ticks{};
	float elapsed_seconds{};

	void tick()
	{
		const Uint64 current_ticks{ SDL_GetPerformanceCounter() };
		const Uint64 delta{ current_ticks - previous_ticks };
		previous_ticks = current_ticks;
		static const Uint64 TICKS_PER_SECOND{ SDL_GetPerformanceFrequency() };
		elapsed_seconds = delta / static_cast<float>(TICKS_PER_SECOND);
	}
};

class GameObject {
private:

	int x, y;
	int w, h;
	int x_v, y_v;		// velocity (consider cv2 vector)
	c2AABB aabb;		// Axis Alingned Bounding Box
	SDL_Rect rectangle; // Draw Rectangle
	int r, g, b, a;		// Render Colors RGBA

public:
	GameObject() {
		x = 0x00;
		y = 0x00;
		w = 0x00;
		h = 0x00;
		x_v = 0x00;
		y_v = 0x00;

		// Render Colors
		r = 0x00;
		g = 0x00;
		b = 0x00;
		a = 0x00;

		// Axis Aligned Bounding Box
		aabb.min = c2V(0.0f, 0.0f);
		aabb.max = c2V(0.0f, 0.0f);// Empty Collider

		// Draw rectangle
		rectangle.x = 0x00;
		rectangle.y = 0x00;
		rectangle.w = 0x00;
		rectangle.h = 0x00;
	}

	void update() {

		// Update World Position
		(x < 0) ? x = 0 : x += x_v;
		(y < 0) ? y = 0 : y += y_v;

		// Keep GameObjects within Level Bounds
		((x + w + x_v) >= LEVEL_WIDTH) ? x = LEVEL_WIDTH - w : x += x_v;
		((y + h + y_v) >= LEVEL_HEIGHT) ? y = LEVEL_HEIGHT - h : y += y_v;

		// Update Collider
		aabb.min = c2V((float)x, (float)y);
		aabb.max = c2V((float)(x + w), (float)(y + h));

		// Update Draw Rectangle
		rectangle.x = x;
		rectangle.y = y;
		rectangle.w = w;
		rectangle.h = h;
	}

	void update(int x_dir, int y_dir) {

		// Update World Position
		(x < 0) ? x = 0 : x += x_v * x_dir;
		(y < 0) ? y = 0 : y += y_v * y_dir;

		// Keep GameObjects within Level Bounds
		((x + w + x_v) >= LEVEL_WIDTH) ? x = LEVEL_WIDTH - w : x += x_v * x_dir;
		((y + h + y_v) >= LEVEL_HEIGHT) ? y = LEVEL_HEIGHT - h : y += y_v * y_dir;

		// Update Collider
		aabb.min = c2V((float)x, (float)y);
		aabb.max = c2V((float)(x + w), (float)(y + h));

		// Update Draw Rectangle
		rectangle.x = x;
		rectangle.y = y;
		rectangle.w = w;
		rectangle.h = h;
	}

	int getX() { return x; }
	void setX(int x) { this->x = x; }
	int getY() { return y; }
	void setY(int y) { this->y = y; }
	int getW() { return w; }
	void setW(int w) { this->w = w; }
	int getH() { return h; }
	void setH(int h) { this->h = h; }
	int getXv() { return x_v; }
	void setXv(int x_v) { this->x_v = x_v; }
	int getYv() { return y_v; }
	void setYv(int y_v) { this->y_v = y_v; }

	c2AABB getc2AABB() { return aabb; }
	void setc2AABB(c2AABB aabb) { this->aabb = aabb; }

	SDL_Rect getSDL_Rect() { return rectangle; }
	void setSDL_Rect(SDL_Rect y_v) { this->rectangle = rectangle; }

	Uint8 getR() { return r; }
	void setR(Uint8 r) { this->r = r; }

	Uint8 getG() { return g; }
	void setG(Uint8 g) { this->g = g; }

	Uint8 getB() { return b; }
	void setB(Uint8 b) { this->b = b; }

	Uint8 getA() { return a; }
	void setA(Uint8 a) { this->a = a; }

	void setRGBA(Uint8 R, Uint8 G, Uint8 B, Uint8 A) {
		this->r = 0;
		this->g = 0;
		this->b = 0;
		this->a = 0;
	}
};

// Basic Camera Class using a SDL_Rect
class Camera {
private:
	SDL_Rect * camera;
	SDL_Rect* lookAt;

	int min_x;
	int min_y;
	int max_x;
	int max_y;

public:
	Camera() {
		camera = new SDL_Rect();
		lookAt = new SDL_Rect();
		this->camera->x = 0;
		this->camera->y = 0;
		this->camera->w = SCREEN_WIDTH;
		this->camera->h = SCREEN_HEIGHT;
	}
	void update(SDL_Rect* focus) {

		//Set the Camera Bounding Box (this box is bound of GameObjects)
		this->camera->x = (focus->x + focus->w / 2) - SCREEN_WIDTH / 2;
		this->camera->y = (focus->y + focus->y / 2) - SCREEN_HEIGHT / 2;

		if (this->camera->x < 0) {
			this->camera->x = 0;
		}
		if (this->camera->y < 0) {
			this->camera->y = 0;
		}
		if (this->camera->x > this->camera->w) {
			this->camera->x = LEVEL_WIDTH - this->camera->w;
		}
		if (this->camera->y > this->camera->h) {
			this->camera->y = LEVEL_HEIGHT - this->camera->h;
		}
	}

	SDL_Rect* getCamera() { return this->camera; }
	SDL_Rect* getLookAt() { return this->lookAt; }

	// The SDL_Rect enclosing the four players
	SDL_Rect* focus(GameObject* gameobjects[]) {
		min_x = gameobjects[0]->getX();
		min_y = gameobjects[0]->getY();
		max_x = gameobjects[0]->getX();
		max_y = gameobjects[0]->getY();

		for (int i = 1; i < PLAYERS; i++)
		{
			(min_x > gameobjects[i]->getX()) ? min_x = gameobjects[i]->getX() : 0;
			(max_x < gameobjects[i]->getX()) ? max_x = gameobjects[i]->getX() + gameobjects[i]->getW() : 0;
			(min_y > gameobjects[i]->getY()) ? min_y = gameobjects[i]->getY() : 0;
			(max_y < gameobjects[i]->getY()) ? max_y = gameobjects[i]->getY() + gameobjects[i]->getH() : 0;
		}

		lookAt->x = min_x;
		lookAt->y = min_y;
		lookAt->w = max_x - min_x;
		lookAt->h = max_y - min_y;

		return lookAt;
	}
};

float calculateScale(float width, float height, float maxWidth, float maxHeight) {
	return max(maxWidth / width, maxHeight / height);
}

void render(Camera* camera, GameObject* players[], GameObject* collidables[])
{
	// Convert from Game World to Screen world using the Offset SDL_Rect

	SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(renderer);

	// Scale the view, it would be better to do this with Lerp
	// Based on your gametime
	float width = (float)camera->getLookAt()->w;
	float height = (float)camera->getLookAt()->h;
	float max_width = ((float)SCREEN_WIDTH / (float)camera->getLookAt()->w) * 0.75;
	float max_height = ((float)SCREEN_HEIGHT / (float)camera->getLookAt()->h) * 0.75;

	float ratio = calculateScale(width, height, max_width, max_height);

#if defined DEBUG
	if (DEBUG >= 3) {
		DEBUG_MSG(std::to_string(ratio));
	}
#endif

	if (ratio > 0.7f)
	{
		scale = (ratio > 1.0f ? 1.0f : ratio);
	}
	else
	{
		scale = 0.65f;
	}

	/*if (focus->w > 0) {
		(SCREEN_WIDTH / focus->w) > 1.0f ? ((scale < 1.0) ? scale += scale_by : scale = 1.0f) : (scale > 0.8 ? scale -= scale_by : scale = 0.8f);
	}*/

	// Sets the render scale for anything rendered with renderer
	SDL_RenderSetScale(renderer, scale, scale);

	// Drawing the Bounding Box with all the players
	// Focus box - camera
	offset->x = focus->x - camera->getCamera()->x;
	offset->y = focus->y - camera->getCamera()->y;
	offset->w = focus->w;
	offset->h = focus->h;

	string info = "Bounding Box:";
	info += " min X:";
	info += std::to_string(offset->x);
	info += " min Y:";
	info += std::to_string(offset->y);
	info += " max X:";
	info += std::to_string(offset->x + offset->w);
	info += " max Y:";
	info += std::to_string(offset->y + offset->h);
	info += " W:";
	info += std::to_string(offset->w);
	info += " H:";
	info += std::to_string(offset->h);
#if defined DEBUG
	if (DEBUG >= 2) {
		DEBUG_MSG(info);
	}
#endif

	//Render Bounding Box
	SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
	SDL_RenderDrawRect(renderer, offset);

	// Little target Box in middle of Focus
	offset->w = CHARACTER_WIDTH;
	offset->h = CHARACTER_HEIGHT;
	offset->x = ((focus->x + focus->w / 2) - offset->w / 2) - camera->getCamera()->x;
	offset->y = ((focus->y + focus->h / 2) - offset->h / 2) - camera->getCamera()->y;

	// Target Box in middle of Focus box
	SDL_RenderDrawRect(renderer, offset);

	// Render the Players GameObjects

	for (int i = 0; i < PLAYERS; i++) {
		SDL_SetRenderDrawColor(renderer,
			players[i]->getR(),
			players[i]->getG(),
			players[i]->getB(),
			players[i]->getA());

		offset->w = players[i]->getW();
		offset->h = players[i]->getH();
		offset->x = players[i]->getX() - camera->getCamera()->x; // - camera
		offset->y = players[i]->getY() - camera->getCamera()->y; // - camera

		// Render Player
		SDL_RenderDrawRect(renderer, offset);

		string info = "Player [" + std::to_string(i);
		info += "]";
		info += "X:";
		info += std::to_string(offset->x);
		info += " Y:";
		info += std::to_string(offset->y);
#if defined DEBUG
		if (DEBUG >= 2) {
			DEBUG_MSG(info);
		}
#endif
	}

	// Render the Collider GameObjects

	for (int i = 0; i < COLLIDERS; i++) {
		SDL_SetRenderDrawColor(renderer,
			collidables[i]->getR(),
			collidables[i]->getG(),
			collidables[i]->getB(),
			collidables[i]->getA());

		offset->w = collidables[i]->getW();
		offset->h = collidables[i]->getH();
		offset->x = collidables[i]->getX() - camera->getCamera()->x; // - camera
		offset->y = collidables[i]->getY() - camera->getCamera()->y; // - camera

		SDL_RenderDrawRect(renderer, offset);

		string info = "Collider [" + std::to_string(i);
		info += "]";
		info += "X:";
		info += std::to_string(offset->x);
		info += " Y:";
		info += std::to_string(offset->y);
#if defined DEBUG
		if (DEBUG >= 2) {
			DEBUG_MSG(info);
		}
#endif
	}

	// Screen Crosshairs
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
	for (int i = 0; i < SCREEN_HEIGHT; i += 4) {
		SDL_RenderDrawPoint(renderer, SCREEN_WIDTH / 2, i);
	}

	for (int i = 0; i < SCREEN_WIDTH; i += 4) {
		SDL_RenderDrawPoint(renderer, i, SCREEN_HEIGHT / 2);
	}

	// Level Crosshairs
	SDL_SetRenderDrawColor(renderer, 0xFF, 0x00, 0x00, 0xFF);
	for (int i = 0; i < LEVEL_HEIGHT; i += 4) {
		SDL_RenderDrawPoint(renderer, LEVEL_WIDTH / 2 - camera->getCamera()->x, i);
	}

	for (int i = 0; i < LEVEL_WIDTH; i += 4) {
		SDL_RenderDrawPoint(renderer, i, LEVEL_HEIGHT / 2 - camera->getCamera()->y);
	}

	SDL_RenderPresent(renderer);
	SDL_Delay(1);
}

// Check Collisions
bool checkCollisions(GameObject* players[], GameObject* collidables[])
{
	bool result = false;

	for (int i = 0; i < PLAYERS; i++)
	{
		for (int j = 0; j < COLLIDERS; j++) {
			result = c2AABBtoAABB(players[i]->getc2AABB(), collidables[j]->getc2AABB());
			if (result) {
#if defined DEBUG
				DEBUG_MSG("Collision");
				collidables[j]->setR(0xFF); // Set to RED
				collidables[j]->setG(0x00); // 0x00
				collidables[j]->setB(0x00); // 0x00
				collidables[j]->setA(0xFF); // 0xFF Alpha
#endif
			}
			else {
#if defined DEBUG
				DEBUG_MSG("-");
#endif

			}
		}
	}
	return result;
}

// Toggle fullscreen

void fullScreenToggle(SDL_Window* Window) {
	Uint32 FullscreenFlag = SDL_WINDOW_FULLSCREEN;
	bool IsFullscreen = SDL_GetWindowFlags(Window) & FullscreenFlag;
	SDL_SetWindowFullscreen(Window, IsFullscreen ? 0 : FullscreenFlag);
	SDL_ShowCursor(IsFullscreen);
}


// Initialize Player GameObjects (Normally in Level Loading)
void initPlayerGameObjects(GameObject* players[]) {

#if defined DEBUG
	DEBUG_MSG("Player Count :" + std::to_string(PLAYERS));
#endif

	// Initialize GameObject Positions
	for (int i = 0; i < PLAYERS; i++) {

		players[i] = new GameObject();

		if (i == 0) {
			players[i]->setRGBA(0x17, 0x5A, 0xC6, 0xFF);
		}
		else if (i == 1) {
			players[i]->setRGBA(0x17, 0xC6, 0x4F, 0xFF);
		}
		else if (i == 2) {
			players[i]->setRGBA(0x7D, 0x33, 0xFF, 0xFF);
		}
		else if (i == 3) {
			players[i]->setRGBA(0xE3, 0x33, 0xFF, 0xFF);
		}
		else {
			players[i]->setRGBA(0x00, 0xAA, 0x00, 0xFF);
		}

		players[i]->setX(rand() % SCREEN_WIDTH + 1);
		players[i]->setY(rand() % SCREEN_HEIGHT + 1);
		players[i]->setW(CHARACTER_WIDTH);
		players[i]->setH(CHARACTER_HEIGHT);
		players[i]->setXv(rand() % MAX + 1);
		players[i]->setYv(rand() % MAX + 1);
	}
}

// Initialize Collider GameObjects (Normally in Level Loading)
void initColliderGameObjects(GameObject* colliders[]) {

#if defined DEBUG
	DEBUG_MSG("Collider Count: " + std::to_string(COLLIDERS));
#endif

	// Initialize Collider GameObject Positions
	for (int i = 0; i < COLLIDERS; i++) {
		colliders[i] = new GameObject();

		colliders[i]->setX(rand() % LEVEL_WIDTH + 1);
		colliders[i]->setY(rand() % LEVEL_HEIGHT + 1);
		colliders[i]->setW(CHARACTER_WIDTH * 5); // 5 times bigger than Character
		colliders[i]->setH(CHARACTER_WIDTH * 5); // 5 times bigger than Character
		colliders[i]->setXv(0); //none moving collider
		colliders[i]->setYv(0); //none moving collider
	}
}

void update(Camera* camera, GameObject* players[], GameObject* collidables[], int x_dir, int y_dir) {
	// Update the GameObjects (this is just moving them about, this will
	// Normally be in Input Handler
	for (int i = 0; i < PLAYERS; i++) {
		players[i]->update(x_dir, y_dir);
	}

	for (int i = 0; i < COLLIDERS; i++) {
		collidables[i]->update();
	}

	// SDL_Rect to focus on
	focus = camera->focus(players);

	// Update Camera based on new focus
	camera->update(focus);
}

void gameloop(Camera* camera, GameObject* players[], GameObject* collidables[], int x_dir, int y_dir) {
	update(camera, players, collidables, x_dir, y_dir);
	checkCollisions(players, collidables);
	render(camera, players, collidables);
}

int main(int argc, char* args[]) {

	const int UPDATE_FREQUENCY{ 60 };
	const float CYCLE_TIME{ 1.0f / UPDATE_FREQUENCY };

	static Timer system_timer;
	float accumulated_seconds{ 0.0f };

	srand((unsigned int)time(NULL));

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Error Initializing %s\n", SDL_GetError());
		return 1;
	}

	window = SDL_CreateWindow(
		"SDL Shader Follow Camera",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SDL_WINDOW_SHOWN
	);


	if (window == NULL) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	if (renderer == NULL)
	{
		fprintf(stderr, "could not create renderer: %s\n", SDL_GetError());
		return 1;
	}

	// Initialize Focus, where camera will look
	focus = new SDL_Rect();

	// Create GameObjects the Players
	GameObject* players[PLAYERS];

	// Set positions for Player GameObjects
	initPlayerGameObjects(players);

	// Create GameObjects the Colliders
	GameObject* collidables[COLLIDERS];

	// Set positions for Collider GameObjects
	initColliderGameObjects(collidables);

	// Initialize Camera
	Camera* camera = new Camera();

	// Initialize Offset
	offset = new SDL_Rect();

	// Initialize Game
	initPlayerGameObjects(players);
	initColliderGameObjects(collidables);

	bool Exit = false;

	int x_dir = 0;
	int y_dir = 0;

	while (!Exit) {

		system_timer.tick();
		accumulated_seconds += system_timer.elapsed_seconds;

		if (std::isgreater(accumulated_seconds, CYCLE_TIME))
		{
			accumulated_seconds = -CYCLE_TIME;
			gameloop(camera, players, collidables, x_dir, y_dir);
		}

		{SDL_Event Event;
		SDL_WaitEvent(&Event);
		if (Event.type == SDL_KEYDOWN) {
			switch (Event.key.keysym.sym) {
			case SDLK_f: fullScreenToggle(window); break;
			case SDLK_q: Exit = true; break;
			case SDLK_UP:
				x_dir = 0;
				y_dir = -1;
				break;
			case SDLK_DOWN:
				x_dir = 0;
				y_dir = 1;
				break;
			case SDLK_LEFT:
				x_dir = -1;
				y_dir = 0;
				break;
			case SDLK_RIGHT:
				x_dir = 1;
				y_dir = 0;
				break;
			case SDLK_r:
				scale = 1.0f; // Reset Scale
				initPlayerGameObjects(players);
				initColliderGameObjects(collidables);
				break;
			default:
				x_dir = 0;
				y_dir = 0;
			}
		}
		}

	}

	// Cleanup
	delete(camera);
	delete(focus);
	delete(offset);

	delete[] & players;
	delete[] & collidables;

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}